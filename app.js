const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const apiRoutes = require('./routes/apiRoutes');

const app = express();

app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use('/api', apiRoutes);

app.listen(8080, () => {
    console.log('Server is runnig!');
});