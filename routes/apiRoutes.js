const { Router } = require('express');
const path = require('path');
const fs = require('fs');

const dirPath = path.join(__dirname, '..', 'files');

const router = Router();

router.post('/files', (request, response) => {
    const { filename, content } = request.body;
    const allowedExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

    if (!fs.existsSync(dirPath)) {
        fs.mkdir(dirPath, (err) => {
            if (err) {
                return res
                    .status(500)
                    .json({
                        message: 'Server error'
                    });
            }
        });
    }

    if (!filename) {
        response
            .status(400)
            .json({ message: 'Please specify \'filename\' parameter' });
    }

    if (!content) {
        response
            .status(400)
            .json({ message: 'Please specify \'content\' parameter' });
    }

    if (!allowedExtensions.includes(path.extname(filename))) {
        response
            .status(400)
            .json({
                message: 'This extension isn\'t allowed'
            });
    } else {
        fs.writeFile(path.join(dirPath, filename), content, err => {
            if (err) {
                response
                    .status(500)
                    .json({ message: 'Server Error' });
            } else {
                response
                    .status(200)
                    .json({ message: 'File created successfully' });
            }
        });
    }
});

router.get('/files', (request, response) => {
    fs.readdir(dirPath, (err, content) => {
        if (err) {
            return response
                .status(500)
                .json({ message: 'Server error' });
        }

        if (content.length === 0) {
            response
                .status(400)
                .json({ message: 'Client error' });
        } else {
            response
                .status(200)
                .json({
                    message: 'Success',
                    files: content
                });
        }
    });
});

router.get('/files/:filename', (request, response) => {
    const filename = request.params.filename;

    fs.readFile(path.join(dirPath, filename), 'utf-8', (err, content) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return response
                    .status(400)
                    .json({ message: `No file with ${filename} filename found` });
            } else {
                response
                    .status(500)
                    .json({ message: 'Server error' });
            }
        }

        fs.stat(path.join(dirPath, filename), (err, stats) => {
            if (err) {
                response
                    .status(500)
                    .json({ message: 'Server error' });
            } else {
                response
                    .status(200)
                    .send({
                        message: "Success",
                        filename,
                        content,
                        extension: path.extname(filename).split('.')[1],
                        uploadedDate: stats.birthtime
                    })
            }
        });
    });
});

router.delete('/files/:filename', (request, response) => {
    const filename = request.params.filename;

    fs.unlink(path.join(dirPath, filename), (err) => {
        if (err) {
            if (err.code === 'ENOENT') {
                response
                    .status(400)
                    .json({ message: `No file with ${filename} filename found` });
            } else {
                response
                    .status(500)
                    .json({ message: 'Server error' });
            }
        } else {
            response
                .status(200)
                .json({ message: `File with ${filename} filename was deleted` });
        }
    });
});

module.exports = router;